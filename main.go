package main

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"github.com/mtfelian/cli"
)

// mustDecode is similar to decode() but panics on error
func mustDecode(source string) string {
	decoded, err := decode(source)
	if err != nil {
		panic(err)
	}
	return decoded
}

// mustJSONMarshal is similar to json.Marshal() but panics on error and works with string instead of []byte
func mustJSONMarshal(source string) string {
	b, err := json.Marshal(source)
	if err != nil {
		panic(err)
	}
	return string(b)
}

// decode source string
func decode(source string) (string, error) {
	decodedString, err := base64.StdEncoding.DecodeString(source)
	if err != nil {
		return "", err
	}

	gzipReader, err := gzip.NewReader(strings.NewReader(string(decodedString)))
	if err != nil {
		return "", err
	}

	decompressedBytes, err := ioutil.ReadAll(gzipReader)
	if err != nil {
		return "", err
	}

	return string(decompressedBytes), nil
}

func main() {
	inputBytes, err := ioutil.ReadFile("./input.txt")
	if err != nil {
		cli.Println("{R|Failed to read input file: %v{0|", err)
		os.Exit(1)
	}

	decodedRecord, err := decode(string(inputBytes))
	if err != nil {
		cli.Println("{R|Failed to decode input record: %v{0|", err)
		os.Exit(1)
	}

	cli.Println("{Y|Decoding record succesful.{0|")

	b64Regex := regexp.MustCompile(`"data":"([\w+/]+=*)"`)
	i := 0
	decodedContext := b64Regex.ReplaceAllStringFunc(decodedRecord, func(s string) string {
		i++
		cli.Println("{Y|Decoding context: {R|%d{0|", i)
		return `"data":` + mustJSONMarshal(mustDecode(strings.TrimRight(strings.TrimLeft(s, `"data":"`), `"`)))
	})
	cli.Println("{G|OK.{0|")

	var indentedJSON bytes.Buffer
	if err := json.Indent(&indentedJSON, []byte(decodedContext), "", "  "); err != nil {
		cli.Println("{R|Failed to indent JSON: %v{0|", err)
		os.Exit(1)
	}
	convenienceJSON := strings.Replace(indentedJSON.String(), `\"`, `"`, -1)

	fmt.Println(convenienceJSON)
}
